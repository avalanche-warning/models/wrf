import datetime
import glob
import netCDF4
import os
import pandas as pd
import pytz
import wrf

def _closest_time_index(dates: list, request_date):
    """Given a list of dates find the index closest to a certain time stamp."""
    request_date = pd.to_datetime(request_date)
    idx_closest = min(range(len(dates)),
        key=lambda idx: abs(dates[idx] - request_date))
    return idx_closest

def _extract_param(wrf_file, lat, lon, param, sdate, edate=None):
    with netCDF4.Dataset(wrf_file) as wrffile:
        if edate is None:
            wrf_dates = get_dates(wrf_file)
            timeidx = _closest_time_index(wrf_dates, sdate)
        else:
            timeidx = wrf.ALL_TIMES

        x_y = wrf.ll_to_xy(wrffile, lat, lon)
        xx, yy = x_y[0], x_y[1]
        var = wrf.getvar(wrffile, param, timeidx=timeidx).sel(west_east=xx, south_north=yy)
    return var

def wrf_extract(wrf_folders, lat, lon, params, domain, sdate, edate=None, pick_any=False):
    wrf_file = find_wrf_file(wrf_folders, sdate, domain, pick_any)
    if not wrf_file:
        return None
    all_vars = {}
    for param in params:
        var = _extract_param(wrf_file, lat, lon, param, sdate, edate)
        all_vars[param] = var
    return all_vars

def get_dates(wrf_file, tz="utc"):
    with netCDF4.Dataset(wrf_file) as wrffile:
        wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).tz_localize(tz).to_pydatetime() for dt in wrf_dates]
    return wrf_dates

def _find_closest_wrf_file(wrf_folders, date, domain, pick_any=False):
    smallest_tdiff = None
    closest_file = None
    for folder in wrf_folders:
        files = glob.glob(os.path.join(folder, f"*_{domain}.nc"))
        files.sort()
        if not files:
            continue
        for file in files:
            run_date = datetime.datetime.strptime(file.split("_")[2], "%Y-%m-%dT%H:%M:%S")
            run_date = pytz.UTC.localize(run_date)
            if run_date > date and not pick_any:
                continue
            tdiff = (date - run_date)
            if smallest_tdiff is None or tdiff < smallest_tdiff:
                smallest_tdiff = tdiff
                closest_file = file
    return closest_file, smallest_tdiff

def find_wrf_file(wrf_folders, date, domain, pick_any=False):
    closest_file, tdiff = _find_closest_wrf_file(wrf_folders, date, domain, pick_any)
    if pick_any:
        print("[W] pick_any is True, which is for debugging only!")
        return closest_file
    if not closest_file:
        return None
    if tdiff.days > 7: # TODO: switch to lead time from config
        return None
    dates = get_dates(closest_file)
    if date >= dates[0] and date <= dates[-1]:
        return closest_file
    else:
        return None
