################################################################################
# Copyright 2023 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import branca
import cartopy
import cartopy.mpl.geoaxes
import folium
import math
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from matplotlib.colors import LinearSegmentedColormap
import netCDF4
import numpy as np
import os
#import pyproj
from rasterio.plot import show
import xarray
import wrf

# global plotting configuration:
plt.rcParams["figure.figsize"] = (20, 20)
plt.rcParams["font.size"] = 22

class BindColormap(branca.element.MacroElement):
    """Binds a colormap to a given layer."""
    def __init__(self, layer: folium.raster_layers.ImageOverlay,
            colormap: branca.colormap.StepColormap, show: bool=True):
        super(BindColormap, self).__init__()
        self.layer = layer
        self.colormap = colormap
        self._init_show = u"block" if show else u"none"

        # https://stackoverflow.com/questions/56164535/adding-colormap-legend-to-folium-map
        _template = u"""
        {% macro script(this, kwargs) %}
            {{this.colormap.get_name()}}.svg[0][0].style.display = '"""
        _template = _template + self._init_show
        _template = _template + u"""';
            {{this._parent.get_name()}}.on('overlayadd', function (eventLayer) {
                if (eventLayer.layer == {{this.layer.get_name()}}) {
                    {{this.colormap.get_name()}}.svg[0][0].style.display = 'block';
                }});
            {{this._parent.get_name()}}.on('overlayremove', function (eventLayer) {
                if (eventLayer.layer == {{this.layer.get_name()}}) {
                    {{this.colormap.get_name()}}.svg[0][0].style.display = 'none';
                }});
        {% endmacro %}
        """
        self._template = branca.element.Template(_template)

def _pfile(fname: str, plot_dir: str=None) -> str:
    """Helper function to get output file path."""
    if not plot_dir:
        plot_dir = "./tmp_data/plots"
    os.makedirs(plot_dir, exist_ok=True)
    return f"{plot_dir}/{fname}.png"

#def _spherical_wgs84(dataset, lats, lons):
#    wrf_proj = pyproj.Proj(
#        proj="lcc", # Lambert Conformal Conic
#        lat_1=dataset.TRUELAT1, lat_2=dataset.TRUELAT2, # Cone intersects with the sphere
#        lat_0=dataset.MOAD_CEN_LAT, lon_0=dataset.STAND_LON, # Center point
#        a=6370000, b=6370000) # earth is modelled as sphere by WRF
#    wgs_proj = pyproj.Proj(proj='latlong', datum='WGS84')
#    transformer= pyproj.Transformer.from_proj(wrf_proj, proj_to=wgs_proj)
#    lats84, lons84 = transformer.transform(lons, lats)
#    return (lats84, lons84)

def _get_latlons(param: xarray.DataArray) -> tuple:
    """Extract latitudes and longitudes for WRF domain."""
    lats, lons = wrf.latlon_coords(param)
    lats = wrf.to_np(lats)
    lons = wrf.to_np(lons)
#    lats, lons = _spherical_wgs84(dataset, lats, lons)
    return (lats, lons)

def _4d_select(param: xarray.DataArray, verbose=True) -> xarray.DataArray:
    """Select layer of 4d variable (time, lat, lon, height/z).

    So far this is simply the highest or lowest level.
    If the layers are staggered, compute the mean and destagger.
    """
    if "bottom_top_stag" in param.dims:
        idx = param.dims.index("bottom_top_stag")
        plot_data = wrf.destagger(param, stagger_dim=idx) # average over below and above level
        try:
            plot_data = plot_data[0, :, :]
            if verbose:
                print("[W] Reduced elevation-dependent variable to lowest height data.")
        except IndexError:
            pass # it was not 4d after all
    elif "soil_layers_stag" in param.dims:
        idx = param.dims.index("soil_layers_stag")
        plot_data = wrf.destagger(param, stagger_dim=idx) # average over below and above level
        try:
            plot_data = plot_data[0, :, :]
            if verbose:
                print("[W] Reduced depth-dependent variable to ground level data.")
        except IndexError:
            pass
    else:
        if "bottom_top" in param.dims:
            param = param.isel(bottom_top=1)
            if verbose:
                print("[W] Reduced elevation-dependent variable to lowest available height.")
        plot_data = wrf.to_np(param)
    return plot_data

def _create_range(data: xarray.DataArray, count: int=10, step: float=None) -> np.ndarray:
    """Create a range of numbers from a dataset's minimum and maximum values.

    Args:
        data: Matrix containing the parameter's values.
        count: Return this many numbers (default: 10).
        step: Instead of a fixed number of values, step forward from the minimum
            with this step size.
    """
    mini = data.values.min()
    maxi = data.values.max()
    if mini == maxi: # force contour level bounds if there are none in the data
        if mini == 0:
            mini = 0
            maxi = 1
        else:
            mini = mini - mini/100
            maxi = maxi + maxi/100
    if maxi > 100:
        mini = math.floor(data.values.min() / 10) * 10 # round down to nearest 10
        maxi = round(data.values.max(), -1) # round up to nearest 10
    if not step: # count mode
        step = (maxi - mini) / count

    rg = np.arange(mini, maxi, step)
    np.append(rg, maxi) # cover the last data point
    return rg

def _get_bbox(lats: np.ndarray, lons: np.ndarray) -> list:
    """Get bounding box of data coverage area as needed by the folium map."""
    bbox=[ [float(lats.min()), float(lons.min())], [float(lats.max()), float(lons.max())] ]
    return bbox

def _get_folium_cmap(colormap: LinearSegmentedColormap,
        levels: np.ndarray, title: str, to_step=True) -> branca.colormap.LinearColormap:
    """Re-compute a branca colormap from a matplotlib colormap and stretch it."""
    index_list = levels.tolist() # these are the individual numbers that need colors
    # map the needed values to [0, 1]:
    index_list = [(pt - levels[0]) / (levels[-1] - levels[0]) for pt in index_list]
    # Use the remapped values in colormap lookup. This way if our values are all within e. g.
    # the last part of the colormap we remap it to the full length of it:
    cmap_list = colormap(index_list).tolist()
    
    cmap_folium = branca.colormap.LinearColormap(
        cmap_list, index=levels, vmin=levels[0], vmax=levels[-1], caption=title)
    if to_step: # convert linear to step cm to match contours
        cmap_folium = cmap_folium.to_step(len(levels) - 1)
    return cmap_folium

def _add_folium_cmap(fmap: folium.Map, raster_layer: folium.raster_layers.ImageOverlay,
        colormap: LinearSegmentedColormap, levels: np.ndarray, title: str,
        show: bool=False, to_step: bool=True):
    """Add a colorbar to the web page and link it to its layer."""
    cmap_folium = _get_folium_cmap(colormap, levels, title, to_step)
    fmap.add_child(cmap_folium)
    fmap.add_child(BindColormap(raster_layer, cmap_folium, show=show)) # toggle cmap visibility with map

def _set_axis(param: xarray.DataArray) -> cartopy.mpl.geoaxes.GeoAxesSubplot:
    """Read WRF projection parameters and set the axis accordingly."""
    cart_proj = wrf.get_cartopy(param) # cartopy mapping object
    ax = plt.axes(projection=cart_proj)
    ax.set_xlim(wrf.cartopy_xlim(param)) # map bounds
    ax.set_ylim(wrf.cartopy_ylim(param))
    return ax

def _add_raster_layer(fmap: folium.Map, plot_file: str, bbox: list, title: str,
        show: bool=False) -> folium.raster_layers.ImageOverlay:
    """Add image from the file system to a given web map.

    Generally speaking we use the flexibility of whichever plotting tools we
    like and save an image to the file system. Then, we load the image from
    there for display using the reference bounding box we have computed from
    the dataset beforehand. This approach has the added benefit of being able
    to control the resultion and thus HTML page size easily and globally."""
    raster_layer = folium.raster_layers.ImageOverlay(
        image=plot_file,
        bounds=bbox,
        origin="lower",
        opacity=0.8,
        show=show,
        name=title
    ).add_to(fmap)
    return raster_layer

def _make_description(param: xarray.DataArray, description: str=None, units: str=None,
        prefix: str=None, postfix: str=None) -> str:
    """Get a parameter's description with the possibility to override it and/or the units."""
    if not description:
        description = param.description.capitalize()
    if not units:
        units = param.units
    plot_title = description
    if units and units != "-":
        plot_title = plot_title + f" ({units})"
    if prefix:
        plot_title = prefix + plot_title
    if postfix:
        plot_title = plot_title + postfix
    return plot_title

def plot_generic(datafile: str, fmap: folium.Map, parameter: str, levels: np.ndarray=None,
        description: str=None, units: str=None, step: float=None, colormap: str="jet",
        show: bool=False, plot_dir: str=None, name_prefix: str=None, name_postfix: str=None):
    """Plot a WRF dataset's parameter for which we prepare no special output."""
    _plot_file = _pfile(parameter)

    with netCDF4.Dataset(datafile) as dataset:
        param = wrf.getvar(dataset, parameter)
        lats, lons = _get_latlons(param)

    fig = plt.figure()
    _set_axis(param)
   
    if not levels:
        levels = _create_range(param, step=step)
    plot_data = _4d_select(param)
    colormap = get_cmap(colormap, lut=len(levels))

    plt.contourf( # surface contours
        lons, lats, plot_data,
        levels=levels,
        transform=cartopy.crs.PlateCarree(),
        cmap=get_cmap(colormap)
    )

    plt.savefig(_plot_file, bbox_inches="tight", pad_inches=0, transparent=True)
    plt.close(fig)
    bbox = _get_bbox(lats, lons)

    plot_title = _make_description(param, description, units, name_prefix, name_postfix)
    raster_layer = _add_raster_layer(fmap, _plot_file, bbox, plot_title, show=show)
    _add_folium_cmap(fmap, raster_layer, colormap, levels, plot_title, show=show, to_step=False)

def plot_generic_vars(datafile: str, fmap: folium.Map, show: bool=False,
        name_prefix: str=None, name_postfix: str=None):
    """Run through list of observables to produce generic plots for.

    Available parameters (add any to the list below):
    http://rccdp.unl.edu/portal/WRF_Variable_Table.pdf
    https://wrf-python.readthedocs.io/en/latest/diagnostics.html
    """
    #[parameter-id, units, description, colormap]
    wrf_observables = [ # set to None to auto-choose
        ["T2", None, "Temperature 2 m above ground", "jet"],
        ["TSK", None, None, "jet"],
        ["SNOWH", None, None, "rainbow"],
        ["QSNOW", "kg/kg", None, "rainbow"],
        ["GRAUPELNC", None, "Daily total graupel", "turbo"],
        ["ALBEDO", None, None, "afmhot"],
        ["SNOALB", None, None, "afmhot"],
        ["U10", None, "U wind component (E-W, positive is W)", "RdBu"],
        ["V10", None, "V wind component (N-S, positive is S)", "RdBu"],
        ["W", None, "Z wind component (bottom-top, pos. is upward)", "RdBu"],
        ["P", None, None, "gist_ncar"],
        ["TSLB", None, None, "jet"],
    ]

    for param in wrf_observables:
        p_name = param[0]
        p_unit = param[1]
        p_desc = param[2]
        p_cols = param[3]
        plot_generic(datafile, fmap, parameter=p_name, units=p_unit,description=p_desc,
            colormap=p_cols, show=False, name_prefix=name_prefix, name_postfix=name_postfix)

def plot_snow_height(datafile: str, fmap: folium.map, show: bool=False,
        name_prefix: str=None, name_postfix: str=None):
    """Plot WRF estimated snow height."""
    param = "SNOWH"
    plot_generic(datafile, fmap, param, step=0.01, show=show,
        name_prefix=name_prefix, name_postfix=name_postfix)

def plot_sealevel_pressure(datafile: str, fmap: folium.Map, plot_dir: str=None,
        show: bool=False, name_prefix: str=None, name_postfix: str=None):
    """Plot the reduced sealevel pressure."""
    _plot_file = _pfile("sea_level_pressure", plot_dir)

    with netCDF4.Dataset(datafile) as dataset:
        param = wrf.getvar(dataset, "slp")
    param = wrf.smooth2d(param, 3, cenweight=4) # can be noisy over mountainous terrain

    lats, lons = _get_latlons(param)
    fig = plt.figure()
    _set_axis(param)

    levels = _create_range(param)
    colormap = get_cmap("ocean", lut=len(levels))

    vw_contours = plt.contour( # contour outlines
        lons, lats, wrf.to_np(param),
        levels=levels,
        colors="black",
        transform=cartopy.crs.PlateCarree() # transform to WSG 84 for generic mapping
    )
    plt.clabel(vw_contours, inline=1, fmt="%i")
    plt.contourf(
        lons, lats, wrf.to_np(param), # filled contours
        levels=levels,
        transform=cartopy.crs.PlateCarree(),
        cmap=get_cmap(colormap)
    )

    plt.savefig(_plot_file, bbox_inches="tight", pad_inches=0, transparent=True)
    plt.close(fig)

    bbox = _get_bbox(lats, lons)
    plot_title = _make_description(param, prefix=name_prefix, postfix=name_postfix)
    raster_layer = _add_raster_layer(fmap, _plot_file, bbox, plot_title, show=show)
    _add_folium_cmap(fmap, raster_layer, colormap, levels, plot_title, show=show)

def plot_wind_pressure_interpol(datafile: str, fmap: folium.map, level: float=500,
        plot_dir: str=None, show: bool=False, name_prefix: str=None,
        name_postfix: str=None):
    """Plot wind speed for an interpolated pressure level."""
    _plot_file = _pfile("wind_pressure_interpol", plot_dir)

    with netCDF4.Dataset(datafile) as dataset:
        pp = wrf.getvar(dataset, "pressure")
        zz = wrf.getvar(dataset, "z", units="m")
        uu = wrf.getvar(dataset, "ua", units="km h-1")
        vv = wrf.getvar(dataset, "va", units="km h-1")
        vw = wrf.getvar(dataset, "wspd_wdir", units="km h-1")[0,:]
    # Interpolate geopotential height, u, and v winds to hPa level:
    pp_interpol = wrf.interplevel(zz, pp, level)
    uu_interpol = wrf.interplevel(uu, pp, level)
    vv_interpol = wrf.interplevel(vv, pp, level)
    vw_interpol = wrf.interplevel(vw, pp, level)

    lats, lons = _get_latlons(pp_interpol)
    fig = plt.figure()
    _set_axis(pp)

    # Add the levels geopotential height contours:
    levels = _create_range(pp_interpol, 10)
    colormap = get_cmap("gist_earth", lut=len(levels))
    contours = plt.contour(lons, lats, wrf.to_np(pp_interpol),
        levels=levels,
        colors="black",
        transform=cartopy.crs.PlateCarree()
    )
    plt.clabel(contours, inline=1, fontsize=10, fmt="%i")

    # Add the wind speed contours:
    levels = _create_range(vw_interpol, 10)
    vw_contours = plt.contourf(lons, lats, wrf.to_np(vw_interpol),
        levels=levels,
        cmap=colormap,
        transform=cartopy.crs.PlateCarree()
    )

    # Add wind barbs at every nth position:
    point_spacing = 50
    barbs_length = 10
    plt.barbs(
        wrf.to_np(lons[::point_spacing, ::point_spacing]),
        wrf.to_np(lats[::point_spacing, ::point_spacing]),
        wrf.to_np(uu_interpol[::point_spacing, ::point_spacing]),
        wrf.to_np(vv_interpol[::point_spacing, ::point_spacing]),
        transform=cartopy.crs.PlateCarree(),
        length=barbs_length
    )

    plt.savefig(_plot_file, bbox_inches="tight", pad_inches=0, transparent=True)
    plt.close(fig)

    bbox = _get_bbox(lats, lons)
    plot_title = _make_description(vv_interpol,
        description = f"{level} mb Height (m), Wind Speed",
        prefix=name_prefix, postfix=name_postfix, units="km/h")
    raster_layer = _add_raster_layer(fmap, _plot_file, bbox, plot_title, show=show)
    _add_folium_cmap(fmap, raster_layer, colormap, levels, plot_title, show=show)

def plot_cloud_top_temperature(datafile: str, fmap: folium.Map, plot_dir: str=None,
        show: bool=False, name_prefix: str=None, name_postfix: str=None):
    """Plot top of cloud temperature."""
    _plot_file = _pfile("cloud_top_temperature", plot_dir)

    with netCDF4.Dataset(datafile) as dataset:
        slp = wrf.getvar(dataset, "slp") # surface level pressure
        slp_smoothed = wrf.smooth2d(slp, 3) # can be noisy
        ctt = wrf.getvar(dataset, "ctt") # cloud top temperature
    
    lats, lons = _get_latlons(slp)
    fig = plt.figure()
    ax = _set_axis(slp)

    # Plot pressure contours:
    levels = _create_range(slp_smoothed)
    contours_pp = ax.contour(
        lons, lats, wrf.to_np(slp_smoothed),
        levels=levels,
        colors="white",
        transform=cartopy.crs.PlateCarree(),
        zorder=3,
        linewidths=1.0
    )
    plt.clabel(contours_pp, inline=1, fmt="%i")

    # Plot cloud top temperature contours:
    colormap = get_cmap("Greys")
    levels = _create_range(ctt)
    ax.contourf(
        lons, lats, wrf.to_np(ctt),
        levels=levels,
        cmap=colormap,
        transform=cartopy.crs.PlateCarree(), 
    zorder=2)

    plt.savefig(_plot_file, bbox_inches="tight", pad_inches=0, transparent=True)
    plt.close(fig)

    bbox = _get_bbox(lats, lons)
    plot_title = _make_description(ctt, prefix=name_prefix, postfix=name_postfix)
    raster_layer = _add_raster_layer(fmap, _plot_file, bbox, plot_title, show=show)
    _add_folium_cmap(fmap, raster_layer, colormap, levels, plot_title, show=show)
