#!/usr/bin/env python3

from awwrf import wrf_extract
import datetime
import pandas as pd
import sys

if __name__ == "__main__":
    if len(sys.argv) < 8 or len(sys.argv) > 9:
        raise Exception("[E] Synopsis: python3 extract_wrf_param.py domain wrf_folder out_file param lat lon sdate <edate>")
    domain = sys.argv[1]
    wrf_folder = sys.argv[2].split(",")
    out_file = sys.argv[3]
    params = sys.argv[4].split(",")
    lat = sys.argv[5]
    lon = sys.argv[6]
    sdate = datetime.datetime.strptime(sys.argv[7], "%Y-%m-%dT%H:%M:%S")
    edate = None
    if len(sys.argv) == 9:
        edate = sys.argv[8]
    
    var = wrf_extract.wrf_extract(wrf_folder, lat=lat, lon=lon, 
        params=params, domain=domain, sdate=sdate, edate=edate)
    with open(out_file, "w") as outfile:
        var.to_csv(outfile)

