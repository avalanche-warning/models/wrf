################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script writes config files for WRF and WPS.
# It looks up necessary information in the domain's settings file and
# transports/calculates the values to be suitable for the WRF chain.
# Settings see:
#   https://www2.mmm.ucar.edu/wrf/users/docs/user_guide_v4/v4.2/users_guide_chap3.html
# Michael Reisecker, 2021

import awset
import datetime
import os
from shutil import copy2
from osgeo import ogr
import sys

def wps(start_date: str, res_time: int, res_steps: int, dx: int, dy: int,
        we_points: int, sn_points: int, domain: str):
    """ Write a WPS configuration file """

    with open(os.path.expanduser("~/wrf/templates/namelist.wps.template"), "r") as ifile:
        wpscfg = ifile.read() #read in template file to modify according to domain

    # section &share:
    wpscfg = wpscfg.replace("$INTERVAL", str(res_time * 60 * 60)) # seconds
    iso_date_format = "%Y-%m-%dT%H:%M:%S"
    wrf_date_format = "%Y-%m-%d_%H:%M:%S"
    sdate = datetime.datetime.strptime(start_date, iso_date_format)
    edate = sdate + datetime.timedelta(hours = res_time * res_steps) # end date from sdate and time steps
    wpscfg = wpscfg.replace("$STARTDATE", sdate.strftime(wrf_date_format))
    wpscfg = wpscfg.replace("$ENDDATE", edate.strftime(wrf_date_format))

    # section &geogrid:
    wpscfg = wpscfg.replace("$EXTWE", str(we_points)) #grid points in West-East direction...
    wpscfg = wpscfg.replace("$EXTSN", str(sn_points)) #... and in South-North
    wpscfg = wpscfg.replace("$DX", str(dx)) #x-resolution in meters
    wpscfg = wpscfg.replace("$DY", str(dy)) #y-resolution in meters

    shapefile = awset.get(["domain", "shape", "file"], domain)
    shape_feature = int(awset.get(["domain", "shape", "feature", "id"], domain))

    shape = ogr.Open(shapefile)
    feature = shape.GetLayer(0).GetFeature(shape_feature)

    extent = feature.GetGeometryRef().GetEnvelope() #extent of a feature
    midlat = extent[2] + (extent[3] - extent[2]) / 2
    midlon = extent[0] + (extent[1] - extent[0]) / 2

    wpscfg = wpscfg.replace("$REFLAT", str(midlat)) # midpoint of domain
    wpscfg = wpscfg.replace("$REFLON", str(midlon)) # ARW: longitude of the center-point of the coarse domain
    wpscfg = wpscfg.replace("$STANDLON", str(round(midlon))) # ARW: longitude that is parallel with the y-axis
    wpscfg = wpscfg.replace("$TRUELAT1", str(round(extent[2]))) # ARW: 1st true lat for Lambert projection
    wpscfg = wpscfg.replace("$TRUELAT2", str(round(extent[3])))

    wpscfg = wpscfg.replace("$GEOGPATH", os.path.expanduser("~/wrf/Build_WRF/WPS_GEOG/")) # static data

    with open(os.path.expanduser("~/wrf/Build_WRF/WPS/namelist.wps"), "w") as ofile:
        ofile.write("!Automatically created by write_config.py for WRF/ARW\n\n")
        ofile.write(wpscfg)

def wrf(start_date: str, res_time: int, res_steps: int, dx: int, dy: int,
        we_points: int, sn_points: int, domain: str):
    """ Write a WRF configuration file """

    with open(os.path.expanduser("~/wrf/templates/namelist.input.template"), "r") as ifile:
        wrfcfg = ifile.read()

    # section &time_control:
    iso_date_format = "%Y-%m-%dT%H:%M:%S"
    sdate = datetime.datetime.strptime(start_date, iso_date_format)
    edate = sdate + datetime.timedelta(hours = res_time * res_steps) # end date from sdate and time steps
    tdelta = edate - sdate # ARW also wants the calculation time span
    tdelta_mins, tdelta_secs = divmod(tdelta.seconds, 60)
    tdelta_hours, tdelta_mins = divmod(tdelta_mins, 60)

    wrfcfg = wrfcfg.replace("$RUNDAYS", str(tdelta.days))
    wrfcfg = wrfcfg.replace("$RUNHOURS", str(tdelta_hours))
    wrfcfg = wrfcfg.replace("$RUNMINUTES", str(tdelta_mins))
    wrfcfg = wrfcfg.replace("$RUNSECONDS", str(tdelta_secs))

    wrfcfg = wrfcfg.replace("$STARTYEAR", sdate.strftime("%Y"))
    wrfcfg = wrfcfg.replace("$STARTMONTH", sdate.strftime("%m"))
    wrfcfg = wrfcfg.replace("$STARTDAY", sdate.strftime("%d"))
    wrfcfg = wrfcfg.replace("$STARTHOUR", sdate.strftime("%H"))
    wrfcfg = wrfcfg.replace("$ENDYEAR", edate.strftime("%Y"))
    wrfcfg = wrfcfg.replace("$ENDMONTH", edate.strftime("%m"))
    wrfcfg = wrfcfg.replace("$ENDDAY", edate.strftime("%d"))
    wrfcfg = wrfcfg.replace("$ENDHOUR", edate.strftime("%H"))

    wrfcfg = wrfcfg.replace("$INTERVAL", str(res_time * 60 * 60)) #in seconds

    # section &domains:
    wrfcfg = wrfcfg.replace("$EXTWE", str(we_points)) #grid points West-East
    wrfcfg = wrfcfg.replace("$EXTSN", str(sn_points))
    wrfcfg = wrfcfg.replace("$DX", str(dx)) #x-resolution (m)
    wrfcfg = wrfcfg.replace("$DY", str(dy))

    with open(os.path.expanduser("~/wrf/Build_WRF/WRF/run/namelist.input"), "w") as ofile:
        ofile.write("!Automatically created by write_config.py for WRF/ARW\n\n")
        ofile.write(wrfcfg)

def aux_files(domain):
    """ Write auxiliary files """
    tsfile = awset.get(["wrf", "tslist", "file"], domain) # time series (~ VStations) file
    if tsfile is not None:
        copy2(tsfile, os.path.expanduser("~/wrf/WRF/run/tslist"))
