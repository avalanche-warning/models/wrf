#!/usr/bin/env python3
################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from datetime import datetime, timedelta
import numpy as np
import os
import subprocess
import sys

_wrf_file = "wrfout_d01_{}T12:00:00_{}.nc"
_wrf_archive = "/mnt/storage-box/wrf/archive"

def run_wrf_history(domain, sdate, edate):
    """WRF wrapper to generate historical WRF simulations for a selected period via a single call."""

    os.makedirs(_wrf_archive, exist_ok=True)

    sdate = datetime.strptime(sdate, "%Y-%m-%d")
    edate = datetime.strptime(edate, "%Y-%m-%d")
    date_range = np.arange(sdate, edate + timedelta(days=1), timedelta(days=1)).astype(datetime)
    for date in date_range:
        date_str = datetime.strftime(date, format="%Y-%m-%d")
        fname_wrf = _wrf_file.format(date_str, domain)
        if not os.path.isfile(os.path.join(_wrf_archive, fname_wrf))):
            subprocess.call([sys.executable, "run_wrf_domain.py", domain, "true", date_str])
            current_loc = os.path.join(os.path.expanduser("~wrf"), "wrf", "output", fname_wrf)),
            if os.path.isfile(current_loc):
                shutil.move(curent_loc, os.path.join(_wrf_archive, fname_wrf))
            else:
                print(f"[E] WRF output file not found for domain {domain} and date {date_str}.")
        else:
            print(f"[i] Skipping run because file {fname_wrf} exists in storage.")
    print(f"[i] WRF runs completed from {sdate} to {edate}.")


if __name__ == "__main__":
    if len(sys.argv) != 4:
        raise Exception("[E] Synopsis: python3 run_wrf_history.py <domain> <sdate> <edate>")
    domain = sys.argv[1]
    sdate = sys.argv[2]
    edate = sys.argv[3]
    run_wrf_history(domain, sdate, edate)
