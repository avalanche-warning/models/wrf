#!/usr/bin/env python3
################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script downloads GFS data, runs WRF on it and moves the output.

import awio
import awset
import os
from datetime import datetime
import glob
import shutil
import subprocess
import sys

def run_domain(domain, force_run: bool, sdate=None):
    if not (force_run or bool(awset.getattribute(["wrf"], "enabled", domain))):
        print(f"[i] Skipping WRF run. Reason: not enabled in domain settings file.")
        return

    if not sdate: # operational run for current date
        gfs_dl_dir = ("/home/data/gfs")
        for file in glob.glob(os.path.join(gfs_dl_dir, "gfs.*.f*")):
            if awio.is_new_file(file):
                print(f"[i] Skipping deletion of young GFS file: {file}")
            else:
                os.remove(file)
    else: # historical run
        gfs_dl_dir = os.path.join("/mnt/wrf/archive/gfs", sdate)

    res_time = awset.get(["wrf", "temporal_resolution", "time"], domain, default=3) # hours
    res_steps = awset.get(["wrf", "temporal_resolution", "steps"], domain, default=8)
    if sdate:
        # Only 3-hour resolution available for historical GFS runs
        if int(res_time) % 3 != 0:
            res_time = str(3 * int(int(res_time)/3))
            print(f"[i] Only 3-hour resolution available for historical GFS runs. Temporal resolution adjusted to {res_time}h.")

    download_script_path = "/home/data/query/gfs/get_gfs_run.py"
    subprocess.call(["python3", download_script_path, res_time, res_steps, gfs_dl_dir, sdate])

    gfs_files = glob.glob(os.path.join(gfs_dl_dir, "*.f*"))
    if not gfs_files:
        raise Exception("[E] No GFS data could be downloaded for this WRF run.")
    gfs_cycle = gfs_files[0].split(".")[1][1:3]
    if not sdate:
        cycle_date = datetime.now().strftime(f"%Y-%m-%dT{gfs_cycle}:00:00")
    else:
        cycle_date = datetime.strptime(sdate,"%Y-%m-%d").strftime(f"%Y-%m-%dT{gfs_cycle}:00:00")

    dx = awset.get(["wrf", "spatial_resolution", "dx"], domain, default=5000)
    dy = awset.get(["wrf", "spatial_resolution", "dy"], domain, default=5000)
    we_points = awset.get(["wrf", "spatial_resolution", "we_points"], domain, default=200)
    sn_points = awset.get(["wrf", "spatial_resolution", "sn_points"], domain, default=200)
    print(f"[i] Calling WRF with start date {cycle_date} for domain '{domain}'")
    print(f"[i] It is now {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}")
    print(f"[i] Lead time: {res_steps} steps a {res_time} hours")
    print(f"[i] Spatial resolution: {int(dx) / 1000} km x {int(dy) / 1000} km; {we_points} points in W-E and {sn_points} in S-N")
    subprocess.call(["python3", os.path.expanduser("~/wrf/start_wrf.py"),
        cycle_date, res_time, res_steps, dx, dy, we_points, sn_points, domain, gfs_dl_dir])

    if sdate:
        outfolder = os.path.expanduser("~/wrf/output")
    else:
        outfolder = os.path.expanduser("/mnt/wrf/archive")
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)
    wrf_out_pattern = os.path.join(os.path.expanduser("~/wrf/Build_WRF/WRF/run"), "wrfout_*")
    for ncfile in glob.glob(wrf_out_pattern):
        # e. g. wrfout_d01_2024-02-17_12:00:00.nc
        outfile = "T".join(os.path.basename(ncfile).rsplit("_", 1)) # ISO date format
        outfile = outfile + "_" + domain + ".nc"
        # full file path of destination (not only folder) to overwrite existing:
        shutil.move(ncfile, os.path.join(outfolder, outfile))

if __name__ == "__main__":
    lockfile_path = os.path.expanduser("~/wrf/.wrf_lockfile")
    if os.path.isfile(lockfile_path):
        exit('[E] Refusing to run WRF twice ("~/wrf/.wrf_lockfile" exists)')
    with open(lockfile_path, "w") as lockfile:
        pass # touch file
    try: # catch and re-throw to be able to delete lockfile on errors
        if len(sys.argv) > 4:
            raise Exception("[E] Synopsis: python3 run_wrf_domain.py <domain> [<force_run>] [<sdate>]")
        
        domain = sys.argv[1]
        force_run = False
        sdate = None # start from now
        if len(sys.argv) > 2:
            force_run = bool(sys.argv[2])
        if len(sys.argv) > 3:
            sdate = sys.argv[3]
        run_domain(domain, force_run, sdate)
        os.remove(lockfile_path)
    except (KeyboardInterrupt, Exception) as err:
        os.remove(lockfile_path)
        raise(err)
