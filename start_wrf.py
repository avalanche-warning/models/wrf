#!/usr/bin/env python3
###############################################################################
# Copyright 2021 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This is the control script for the WRF weather model.
# It reads domain information and starts WPS and WRF on the area.
# Michael Reisecker, 2021

import awio
import glob
import os
import subprocess
import sys
import time

import write_config #to create WPS and WRF "namelist" config files

_cpus = 6 # https://gitlab.com/avalanche-warning/models/wrf/-/issues/10

def make_env():
    """ Set up an environment with library and executable paths for WRF """
    wrf_env = os.environ.copy() # point to local netCDF installation
    wrf_env["NETCDF"] = os.path.expanduser("~/wrf/Build_WRF/LIBRARIES/netcdf")

    old_path = ""
    if "LD_LIBRARY_PATH" in wrf_env:
        old_path = wrf_env["LD_LIBRARY_PATH"] + ":" # local installs of pnglib, jasperlib and zlib
    wrf_env["LD_LIBRARY_PATH"] = old_path + os.path.expanduser("~/wrf/Build_WRF/LIBRARIES/grib2/lib")
    old_path = ""
    if "PATH" in wrf_env:
        old_path = wrf_env["PATH"] + ":" # local MPI installation
    wrf_env["PATH"] = old_path + os.path.expanduser("~/wrf/Build_WRF/LIBRARIES/mpich/bin")
    return wrf_env

def run_wps(sdate, resolution, steps, dx, dy, we_points, sn_points, domain, gfs_dir):
    """ Run the WRF Preprocessing System (WPS) """
    wrf_env = make_env()
    nr_cores = _cpus

    # Clear all previous output data:
    del_patterns = ["geo_em.*.nc", "GRIBFILE.*", "FILE:*", "met_em.*.nc"]
    del_patterns = [os.path.expanduser("~/wrf/Build_WRF/WPS/") + pattern for pattern in del_patterns]
    to_del = awio.multidel(*del_patterns)

    # Write WPS config file:
    write_config.wps(sdate, resolution, steps, dx, dy, we_points, sn_points, domain)

    # Pre-process static data for the domain:
    rcode = subprocess.call(["mpirun", "-n", str(nr_cores), "./geogrid.exe"],
        env=wrf_env, cwd=os.path.expanduser("~/wrf/Build_WRF/WPS"),
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    if rcode != 0:
        print(f"[E] Return code of geogrid.exe is: {rcode}")

    # Link the data files to WPS:
    rcode = subprocess.call(["./link_grib.csh", gfs_dir+"/"],
        env=wrf_env, cwd=os.path.expanduser("~/wrf/Build_WRF/WPS"),
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    if rcode != 0:
        print(f"[E] Return code of link_grib.sh is: {rcode}")

    # Link the proper GFS variables table:
    vtable_dst = os.path.expanduser("~/wrf/Build_WRF/WPS/Vtable")
    if os.path.exists(vtable_dst):
        os.remove(vtable_dst)
    os.symlink(os.path.expanduser("~/wrf/Build_WRF/WPS/ungrib/Variable_Tables/Vtable.GFS"), vtable_dst)

    # Process data to intermediate format:
    rcode = subprocess.call("./ungrib.exe",
        env=wrf_env, cwd=os.path.expanduser("~/wrf/Build_WRF/WPS"),
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    if rcode != 0:
        print(f"[E] Return code of ungrib.exe is: {rcode}")

    # Link ARW variables table:
    mettab_dst = os.path.expanduser("~/wrf/Build_WRF/WPS/METGRID.TBL")
    if os.path.exists(mettab_dst):
        os.remove(mettab_dst)
    os.symlink(os.path.expanduser("~/wrf/Build_WRF/WPS/metgrid/METGRID.TBL.ARW"), mettab_dst)

    # Call interpolation routines:
    rcode = subprocess.call(["mpirun", "-n", str(nr_cores), "./metgrid.exe"],
        env=wrf_env, cwd=os.path.expanduser("~/wrf/Build_WRF/WPS"),
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    if rcode != 0:
        print(f"[E] Return code of metgrid.exe is: {rcode}")

def run_wrf(sdate, resolution, steps, dx, dy, we_points, se_points, domain):
    """ Start the WRF model """

    # check if WRF has caused oom-killer to jump into action: grep -i kill /var/log/syslog
    wrf_env = make_env()
    nr_cores = _cpus

    write_config.wrf(sdate, resolution, steps, dx, dy, we_points, se_points, domain)

    # Cleanup:
    del_patterns = ["met_em.*.nc", "wrfinput_d*", "wrfbdy_d*", "rsl.out.*", "rsl.error.*", "wrfout_d*"]
    del_patterns = [os.path.expanduser("~/wrf/Build_WRF/WRF/run/") + pattern for pattern in del_patterns]
    to_del = awio.multidel(*del_patterns)

    # Link meteo data from WPS to WRF:
    metfiles = glob.glob(os.path.expanduser("~/wrf/Build_WRF/WPS/met_em.*.nc"))
    for mfile in metfiles:
        mfile_dst = os.path.join(os.path.expanduser("~/wrf/Build_WRF/WRF/run"), os.path.basename(mfile))
        os.symlink(mfile, mfile_dst)

    # Create initial state of atmosphere and boundary conditions:
    rcode = subprocess.call(["mpirun", "-n", str(nr_cores), "./real.exe"],
        env=wrf_env, cwd=os.path.expanduser("~/wrf/Build_WRF/WRF/run"),
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    if rcode != 0:
        print(f"[E] Return code of real.exe is: {rcode}")

    # Integrate model through time:
    rcode = subprocess.call(["mpirun", "-n", str(nr_cores), "./wrf.exe"],
        env=wrf_env, cwd=os.path.expanduser("~/wrf/Build_WRF/WRF/run"),
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    if rcode != 0:
        print(f"[E] Return code of wrf.exe is: {rcode}")

if __name__ == "__main__":
    if len(sys.argv) > 10:
        raise Exception("[E] Synopsis: python3 run_wrf.py <start_date> <resolution> <steps> <dx> <dy> <we_points> <sn_points> <domain> <gfs_dir>")
    sdate = sys.argv[1]
    resolution = int(sys.argv[2])
    steps = int(sys.argv[3])
    dx = int(sys.argv[4])
    dy = int(sys.argv[5])
    we_points = int(sys.argv[6])
    sn_points = int(sys.argv[7])
    domain = sys.argv[8]
    gfs_dir = sys.argv[9]

    print("[i] ARW is working...")
    stime_wps = time.time()
    run_wps(sdate, resolution, steps, dx, dy, we_points, sn_points, domain, gfs_dir) # start WPS
    elapsed_wps = awio.iso_timediff(stime_wps)
    print("[i] WRF is working...")
    stime_wrf = time.time()
    run_wrf(sdate, resolution, steps, dx, dy, we_points, sn_points, domain) # start WRF
    elapsed_wrf = awio.iso_timediff(stime_wrf)
    print(f"[i] WPS took {elapsed_wps}, WRF took {elapsed_wrf} (together: {awio.iso_timediff(stime_wps)}).")
