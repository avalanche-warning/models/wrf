#!/usr/bin/env bash
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script locally installs specific library versions after they have
# been downloaded.

set -e

if [[ -z "${AWSOME_BASE}" ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "WRF's install_libraries.sh set AWSOME_BASE to $AWSOME_BASE"
fi
source ${AWSOME_BASE}/weather/wrf/setup/set_variables.sh
echo "[i] Installing libraries netcdf, mpich, zlib, libpng, jasper..."

if [ ! -f $DIR/netcdf-c-4.7.2.tar.gz ]; then
	echo "[i] No compressed files left for libraries. Already installed?"
	return
fi

# Install NetCDF-C to "netcdf":
cd $DIR
tar xzvf netcdf-c-4.7.2.tar.gz
rm netcdf-c-4.7.2.tar.gz
cd netcdf-c-4.7.2
./configure --prefix=$DIR/netcdf --disable-dap --disable-netcdf-4 --disable-shared
make install
cd ..

# Install NetCDF-Fortran to "netcdf":
tar xzvf netcdf-fortran-4.5.2.tar.gz
rm netcdf-fortran-4.5.2.tar.gz
cd netcdf-fortran-4.5.2

export CPPFLAGS="$CPPFLAGS -I${NETCDF}/include"
export LDFLAGS="$LDFLAGS -L${NETCDF}/lib"
export LD_LIBRARY_PATH="${NETCDF}/lib"
export LIBS="-lnetcdf"
./configure --prefix=${NETCDF} --disable-shared
make install
cd ..

# Install MPI to "mpich":
tar xzf mpich-4.0.2.tar.gz
rm mpich-4.0.2.tar.gz
cd mpich-4.0.2
# MPICH BUG: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=91731
./configure FFLAGS=-fallow-argument-mismatch FCFLAGS=-fallow-argument-mismatch --prefix=$DIR/mpich
make
make install
cd ..

# Install zlib to "grib2":
tar xzf zlib-1.2.7.tar.gz
rm zlib-1.2.7.tar.gz
cd zlib-1.2.7
./configure --prefix=$DIR/grib2
make
make install
cd ..

# Install libpng to "grib2":
tar xzf libpng-1.2.50.tar.gz
rm libpng-1.2.50.tar.gz
cd libpng-1.2.50
./configure --prefix=$DIR/grib2
make
make install
cd ..

# Install Jasper to "grib2":
tar xzf jasper-1.900.1.tar.gz
rm jasper-1.900.1.tar.gz
cd jasper-1.900.1
./configure --prefix=$DIR/grib2
make
make install
cd ..
