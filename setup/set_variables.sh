#!/usr/bin/env bash
################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Set some local environment variables for building and to locate locally
# installed software.

set -e

export DIR=~/wrf/Build_WRF/LIBRARIES
export CC=gcc
export CXX=g++
export FC=gfortran
export FCFLAGS=-m64
export F77=gfortran
export FFLAGS=-m64
export JASPERLIB=$DIR/grib2/lib
export JASPERINC=$DIR/grib2/include
export LDFLAGS=-L$DIR/grib2/lib
export CPPFLAGS="-I$DIR/grib2/include -DNDEBUG -DgFortran"
export NETCDF=$DIR/netcdf
export PATH=$DIR/netcdf/bin:$PATH
export PATH=$DIR/mpich/bin:$PATH
