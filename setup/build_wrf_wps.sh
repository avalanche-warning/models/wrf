#!/usr/bin/env bash
################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script compiles WRF and WPS after they have been downloaded.
# When calling by hand (i. e. not via the AWSOME installer) choose options 34
# and then 1 (already defaulted) and 1 again in the installation process.

set -e

options=$(getopt -o udr --long reinstall -- "$@")
option_reinstall=false
eval set -- "$options"
while [ : ]; do
	case "$1" in
		-r | --reinstall)
			option_reinstall=true
			shift
			;;
		--) shift;
			break
			;;
	esac
done

if [[ -z "${AWSOME_BASE}" ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "WRF's build_wrf_wps.sh set AWSOME_BASE to $AWSOME_BASE"
fi
source ${AWSOME_BASE}/weather/wrf/setup/set_variables.sh

if [ "$option_reinstall" = true ]; then
	echo "Removing WRF build files as requested via --reinstall"
	if [ -d ~/wrf/Build_WRF/WRF } ]; then
		( cd ~/wrf/Build_WRF/WRF && ./clean )
	fi
	if [ -d ~/wrf/Build_WRF/WPS} ]; then
		( cd ~/wrf/Build_WRF/WPS && ./clean )
	fi
fi

if [ ! -f ~/wrf/Build_WRF/WRF/main/real.exe ]; then
	cd ~/wrf/Build_WRF/WRF
	./configure
	./compile em_real
else
	echo "WRF already compiled."
fi

# Compile pre-processor:
if [ ! -f ~/wrf/Build_WRF/WPS/metgrid/metgrid.exe ]; then
	cd ../WPS
	./configure
	./compile
else
	echo "WPS already compiled."
fi
