#!/usr/bin/env bash
################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script downloads the WRF and WPS source code as well as specific
# versions of needed libraries and some static geo data.

set -e

cd ~
mkdir -p wrf/Build_WRF
cd wrf/Build_WRF

# get the model and preprocessor source codes:
if [ -d "WRF" ]; then
	cd WRF
	git pull
	cd ..
else
	git clone https://github.com/wrf-model/WRF
fi
if [ -d "WPS" ]; then
	cd WPS
	git pull
	cd ..
else
	git clone https://github.com/wrf-model/WPS
fi

#download static geographic data:
if [ -d "WPS_GEOG" ]; then
	echo "Skipping download of GEOG file (exists)."
else
	if [[ ! -z $AWSOME_SMALL ]]; then # small installation requested
		echo "[i] Switching to low res WPS geog file as requested via small installation flag"
		geog_file=geog_low_res_mandatory.tar.gz # useful e. g. for GitLab runners
	else
		geog_file=geog_high_res_mandatory.tar.gz
	fi

	wget --no-verbose https://www2.mmm.ucar.edu/wrf/src/wps_files/${geog_file}
	echo "Decompressing ${geog_file}..."
	tar -xf ${geog_file}
	rm ${geog_file}
fi

#download needed libraries:
if [ ! -d "LIBRARIES" ]; then
	mkdir LIBRARIES
	cd LIBRARIES
	wget --no-verbose https://www.mpich.org/static/downloads/4.0.2/mpich-4.0.2.tar.gz
	wget --no-verbose https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-c-4.7.2.tar.gz
	wget --no-verbose https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-fortran-4.5.2.tar.gz

	wget --no-verbose https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/jasper-1.900.1.tar.gz
	wget --no-verbose https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/libpng-1.2.50.tar.gz
	wget --no-verbose https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/zlib-1.2.7.tar.gz
fi
