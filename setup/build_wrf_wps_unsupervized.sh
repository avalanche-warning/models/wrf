#!/usr/bin/bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script answers installation prompts automatically.

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
scriptfile=$cwd/build_wrf_wps.sh

# answer some questions the WRF setup has:
/usr/bin/expect -c "
spawn $scriptfile
expect \"Enter selection \[1-83\] : \r\"
send -- \"34\r\"
expect \"Compile for nesting?\"
send -- \"1\r\"
expect \"Enter selection \[1-44\] : \r\"
send -- \"1\r\"
expect eof
"
