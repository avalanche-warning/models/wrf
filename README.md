This module configures, runs and visualizes WRF as NWP.
It is part of the AWSOME avalanche forecasting framework.
For details view the project's home at:
https://gitlab.com/avalanche-warning/weather/wrf/-/wikis/home
