#!/usr/bin/env python3
################################################################################
# Copyright 2023 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from awwrf import wrf_plot
import folium
import matplotlib.pyplot as plt
import os
import sys

def produce_map(wrffile, map_out_path=None):
    """Produce a web map with a couple of WRF processed plots."""
    fmap = folium.Map(location=_map_init_location, zoom_start=6, tiles="openstreetmap")

    wrf_plot.plot_sealevel_pressure(wrffile, fmap)
    wrf_plot.plot_generic(wrffile, fmap, "QRAIN", colormap="seismic")
    wrf_plot.plot_wind_pressure_interpol(wrffile, fmap)
    wrf_plot.plot_snow_height(wrffile, fmap, show=True)
    wrf_plot.plot_cloud_top_temperature(wrffile, fmap)
    wrf_plot.plot_generic_vars(wrffile, fmap)

    folium.LayerControl(position="topleft",).add_to(fmap)
    save_map(fmap, map_out_path)

def save_map(fmap, map_path=None):
    """Save interactive HTML map to file system."""
    if not map_path:
        os.makedirs("./plots", exist_ok=True)
        map_path = "./plots/wrf_maps.html"
    fmap.save(map_path)
    print(f"[i] Map saved to {map_path}.")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("[E] Synopsis: ./produce_map.py <wrf_data_file.nc> [<map_out_path>]")
        sys.exit()

    wrffile = sys.argv[1]
    _map_init_location = [47, 11.5]
    produce_map(wrffile)
